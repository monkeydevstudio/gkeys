<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Games;

class priceupd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'priceupd';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $json=json_decode(file_get_contents('http://steammachine.ru/api/goodslist/?v=1&format=json'),1); 
        //  print_r($json);
        foreach ($json['response']['data']['goods'] as $k=>$v) {  
         //print_r($v);
            $price=(isset($v['price']['rub'])?$v['price']['rub']:0);
            $available=$v['available'];
            $params=Games::where('sbid',$v['id_good'])->where('act',1)->select('id','params','act')->first();
            if(isset($params->id)){
                //echo "$params->id\n";
                $par=$params->params;
                //print_r($par);
                $par['price']=$price;
                $par['available']=$available;
                //$params=$par;
                $gid=Games::where('sbid',$v['id_good'])->update(['price'=>$price,'available'=>$available,'params'=>json_encode($par)]);
                echo "sbid=>$v[id_good], stat=>$gid\n";
            }
           
        }
    }
}
