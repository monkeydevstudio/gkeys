<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Games;

class GameParser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'gameparser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();
    }


        
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $games=Games::where('act', 1)->where('status',0)->get();
        $json=json_decode(file_get_contents('http://steammachine.ru/api/goodslist/?v=1&format=json'),1);
        foreach ($games as $k=>$v) {
            $html = new \Htmldom('http://steambuy.com/goods.php?idd='.$v['sbid']);
            $gg=array();
            $attachment=array();
            foreach($html->find('ul.tovar-info-list -> li') as $e){
                $c=$e->plaintext;
                if(preg_match('#Жанры:(.*)#',$c,$o)){
                    $gg['genre']=trim($o[1]);
                }
                if(preg_match('#Платформа:(.*)#',$c,$o)){
                    $gg['platform']=trim($o[1]);
                }
                if(preg_match('#Язык:(.*)#',$c,$o)){
                    $gg['lang']=trim($o[1]);
                }if(preg_match('#Дата выхода:(.*)#',$c,$o)){
                    $gg['release']=trim($o[1]);
                }if(preg_match('#Издатель:(.*)#',$c,$o)){
                    $gg['genre']=trim($o[1]);
                }if(preg_match('#Мультиплеер:(.*)#',$c,$o)){
                    $gg['multiplayer']=trim($o[1]);
                }if(preg_match('#Кооператив:(.*)#',$c,$o)){
                    $gg['coop']=trim($o[1]);
                }
            }

           
            foreach($html->find('div.tovar-description-main -> p') as $desc){
                $gg['description']=$desc->plaintext;
            }

            foreach($html->find('ul.tovar-video-list -> a') as $vid){
                $attachment['videos'][]=$vid->href;
            }
            foreach($html->find('ul.tovar-screenshot-list -> a') as $vid){
                $attachment['images'][]=$vid->href;
            } 
            foreach($html->find('.block-label') as $ops){
                $os[]=$ops->plaintext;
            }
            foreach($html->find('ul.tovar-param-list') as $kkk=>$param){
                $gg['params'][]=$param->innertext; 
            }
            if(isset($gg['params'])){
                foreach ($gg['params'] as $kk=>$vv) {
                    $par=explode('</li>',$vv);
                    foreach ($par as $kkk=>$vvv) {
                        $par2[$kk][$kkk]=explode(':',strip_tags($vvv));
                    }
                    foreach ($par2 as $kkkk=>$vvvv) {
                       
                        foreach ($vvvv as $k1=>$v1) {
                            if(isset($v1[1])){
                                $f=trim($v1[0]);
                                $v2=trim($v1[1]);
                                $gg['additional'][$kk][$f]=$v2;
                            }
                        }
                    }
                
                }
            }
            
            $json=json_decode(file_get_contents('http://steammachine.ru/api/good/?id_good='.$v->sbid.'&v=1&format=json'),1);
            $j=$json['response']['data']['goods'][0];
            $attachment['preview']=$j['img'];
            if(isset($gg['description'])){
                $description=$gg['description'];
            }else{
                $description='';
            }
            if(isset($gg['params'])){
                $params=$gg['params'];
            }else{
                $params='';
            }
            if(isset($gg['release'])){
                $release=$gg['release'];
            }else{
                $release='';
            }
            if(isset($gg['lang'])){
                $lang=$gg['lang'];
            }else{
                $lang='';
            }
            if(isset($gg['platform'])){
                $platform=$gg['platform'];
            }else{
                $platform='';
            }
           
            $arr=array(
                'title'=>$j['name'],
                'price'=>(isset($j['price']['rub'])?$j['price']['rub']:0),
                'sbid'=>$v->sbid,
                'available'=>$j['available'],
                'genre'=>$j['genre'],
                'activation'=>$j['activation'],
                'attachment'=>$attachment,
                'slug'=>str_slug($j['name']),
                'lang'=>$lang,
                'platform'=>$platform,
                'release'=>$release,    
                'description'=>$description,   
                'additional'=>$params,
                'status'=>1    
            );
            $arr['params']=$arr;
            $gid=Games::updateOrCreate(
               [
                'sbid'=>$v->sbid
               ],
               $arr
                
            );
            echo "insert: $gid->id\n";
            sleep(10); 
        }
    }
}
