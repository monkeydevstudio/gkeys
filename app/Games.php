<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Games extends Model
{
    protected $fillable = ['title','sbid','availble','price','genre','activation','slug','params','act','status','platform'];
    public $timestamps = false;
   	//protected $hidden=['params'];
    //public $appends = ['at'];
    public $translatable = ['params'];
    protected $casts = ['params' => 'json'];

    public function setAtAttribute(){
        return is_array($this->params) ? $this->params : (array) json_decode($this->params,1);
    }
}
