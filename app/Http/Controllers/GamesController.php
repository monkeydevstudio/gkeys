<?php
namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use App\Games;
use Response;

class GamesController extends Controller{
    public function paginate($current=1,$url='',$last=1){
        $max=13;
        $items=array();
        if($current<=7){
            for($i=1;$i<$max;$i++){
                if($i==$current){
                    $items['params'][$i]=array('item'=>$i,'active'=>true);
                }else{
                    $items['params'][$i]=array('item'=>$i);
                }
            }
        }
        
        if($current>7 && $current+ceil($max/2)<$last){
            for($i=$current-ceil($max/2)+1;$i<$current+ceil($max/2);$i++){
                if($i==$current){
                    $items['params'][$i]=array('item'=>$i,'active'=>true);
                }else{
                    $items['params'][$i]=array('item'=>$i);
                }
            }
        }

    
        if($current+ceil($max/2)>$last){
            for($i=$current-($max-($last-$current));$i<=$last;$i++){
                if($i==$current){
                    $items['params'][$i]=array('item'=>$i,'active'=>true);
                }else{
                    $items['params'][$i]=array('item'=>$i);
                }
            }
            for($i=$current+1;$i<$last;$i++){
                if($i==$current){
                    $items['params'][$i]=array('item'=>$i,'active'=>true);
                }else{
                    $items['params'][$i]=array('item'=>$i);
                }
            }
        }  
        return $items;   
    }
    public function index(Request $request){
        $apiurl=env('API_URl');
        $page=(isset($_GET['page'])?$_GET['page']:1);   
        $row=json_decode(file_get_contents("http://api.monkeydevstudio.ru/gameshop?commerce&page=$page"),1);
        $row['paginate']=$this->paginate($row['current_page'],$request->path(),$row['last_page']);
        $row['paginate']['prev']=($row['current_page']>7?$row['current_page']-7:1);
        $row['paginate']['next']=($row['current_page']<$row['last_page']-7?$row['current_page']+7:$row['last_page']);    
        $row['paginate']['current']=$row['current_page'];    
        //print_r($row['paginate']);
        return Response::view('410', array(), 410);
        exit;     
        return view('layouts/main',['row'=>$row]);
    }
    public function post($slug){
        $apiurl=env('API_URl');
        $row=json_decode(file_get_contents("http://api.monkeydevstudio.ru/gameshop/$slug?commerce"),1);
        if(!isset($row['error']['code'])){
            return Response::view('410', array(), 410);
            exit;     
            return view('layouts/inner',['row'=>$row]); 
        }else{
            //header("HTTP/1.0 404 Not Found");
            //return view('404')
            return Response::view('404', array(), 404);
            exit;
        }
           
    }
    public function search(Request $request){
    	$apiurl=env('API_URl');
        $gnr=array(
            'arcada'=>'Аркада',
            'simulator'=>'Симулятор',
            'action'=>'Экшен',
            'strategy'=>'Стратегии',
            'role'=>'Ролевые',
            'casual'=>'Казуальные',
            'racing'=>'Гонки',
            'sport'=>'Спорт',
            'online'=>'Онлайн',
            'fighting'=>'Файтинги',
            'adventure'=>'Приключения'
        );
        $s=array();
        $dmn=$request->url();
        $url=str_replace("$dmn",'',$request->fullurl());
        $row=json_decode(file_get_contents("http://api.monkeydevstudio.ru/gameshop/search$url&commerce"),1);
        return Response::view('410', array(), 410);
        exit;  
        return view('layouts/main',['row'=>$row]);
    }
}
