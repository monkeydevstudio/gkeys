<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Games;
use Response;

class SitemapController extends Controller{
    public function index(){
        header("Content-Type: text/xml");
        $row=Games::where('act',1)
            ->where('status',1)
            ->get();

        return Response::view('xml.sitemap2',['row'=>array()])->header('Content-Type', 'application/xml');
        //return Response::view('xml.sitemap',['row'=>$row])->header('Content-Type', 'application/xml');
    }
}
