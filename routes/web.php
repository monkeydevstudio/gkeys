<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('index');
});*/

Route::get('/help', function () {
    return view('help');
});
Route::get('/discount',function () {
    return view('discount');
});
Route::get('/garants', function () {
    return view('garants');
});
Route::get('/favorites', function () {
    return view('favorites');
});
Route::get('/game', function () {
    header("HTTP/1.1 301 Moved Permanently");
	header("Location: /");
	exit;
	
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/sitemap.xml', 'SitemapController@index'); 
//Route::get('/', 'GamesController@index');
Route::get('/search', 'GamesController@search');
Route::get('/{slug}', 'GamesController@post');


