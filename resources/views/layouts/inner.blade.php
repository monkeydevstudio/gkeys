@extends('game')
@section('content')
<?php $row=$row['params']; $seo['description']=mb_substr($row['description'],0,167).'...'; ?>
 @section('pageTitle', $row['title'])
 @section('description',$seo['description'])
 @section('service','STEAM')
 @section('image',$row['attachment']['preview'])
 @section('url','http://gkeys.store/'.$row['slug'])

	<style>.label{color:#000!important;font-size:100%!important;background-color: #fff!important} li{list-style-type: none; } ul { margin-left: 0; /* Отступ слева в браузере IE и Opera */ padding-left: 0; /* Отступ слева в браузере Firefox, Safari, Chrome */ }.block-heading{margin-top:25px;}ol{padding-left: 10px}</style>
	<div class="col-md-9">
		<div class="row">
			
			<div class="col-sm-12 col-md-12">
				<div class="page-header">
					  <h1>{{$row['title']}}</h1>
					 
					</div>
				<div class="col-sm-12 col-md-12 "><p>Ищешь где купить лицензионный ключ {{$row['title']}}? Ты пришел по адресу!
Оплатив данный товар, вы получите лицензионную копию {{$row['title']}} для активации в системе e-mail, указанный в процессе покупки.</p></div>	
				@if(isset($row['attachment']['images']))
				<div class="fotorama"  data-nav="thumbs" data-allowfullscreen="true" data-autoplay="true" data-shuffle="true">
					@foreach ($row['attachment']['images'] as $k=>$v)
						<a href="{{$v}}" data-thumb="{{$v}}"></a>
					@endforeach
				</div>
				@endif
				<br clear="left"/>
				<div class="col-sm-12 col-md-12">
					<p>{{$row['description']}}</p>
				</div>
				@if($row['additional'])
				<div class="col-sm-12 col-md-12">
					<h3 class="block-heading">Системные требования:</h3>
					@foreach ($row['additional'] as $k=>$v)
						<div class="col-sm-4 col-md-4"><ol>{!!$v!!}</ol></div>
					@endforeach
				</div>
				@endif
				<div class="col-sm-12 col-md-12"><h3 class="block-heading">Инструкция по активации:</h3><ol class="tovar-activation-list"><li>Если не установлен Steam клиент, <a href="http://store.steampowered.com/about/" target="_blank">скачайте</a> и установите его.</li><li>Войдите в свой аккаунт Steam или <a href="https://store.steampowered.com/login/">зарегистрируйте</a> новый, если у вас его еще нет.</li><li>Перейдите в раздел «Игры» и выберите там «Активировать через Steam...».</li><li>Введите ключ активации (для его получения необходимо <strong>купить {{$row['title']}}</strong>).</li><li>После этого игра отобразится в разделе «Библиотека», и вы сможете <strong><noindex>скачать</noindex> {{$row['title']}}</strong>.</li><br> <p style="color:red; font-size: 10px">ВНИМАНИЕ! Способ передачи игры покупателю (ключ или гифт) может меняться без предварительного предупреждения. Вы в любом случае получаете игру на свой STEAM-аккаунт. </p> </ol></div>
				<div class="col-sm-12 col-md-12"><p>* This product can be activated and played only in: Russia, Ukraine, Azerbaijan, Belarus, Kazakhstan, Kyrgyzstan, Tajikistan, Turkmenistan</p>
<p>* Активировать и играть в данный продукт можно только на территории России, Украины, Азербайджана, Беларуси, Казахстана, Киргизии, Таджикистана, Туркменистана.</p></div>
				<div><script id="CSAg3ohJ-wH7wMDA">if (window.relap) window.relap.ar('CSAg3ohJ-wH7wMDA');</script></div>
				<div>
					<div id="hypercomments_widget"></div>
					<script type="text/javascript">
					_hcwp = window._hcwp || [];
					_hcwp.push({widget:"Stream", widget_id: 85329});
					(function() {
					if("HC_LOAD_INIT" in window)return;
					HC_LOAD_INIT = true;
					var lang = (navigator.language || navigator.systemLanguage || navigator.userLanguage || "en").substr(0, 2).toLowerCase();
					var hcc = document.createElement("script"); hcc.type = "text/javascript"; hcc.async = true;
					hcc.src = ("https:" == document.location.protocol ? "https" : "http")+"://w.hypercomments.com/widget/hc/85329/"+lang+"/widget.js";
					var s = document.getElementsByTagName("script")[0];
					s.parentNode.insertBefore(hcc, s.nextSibling);
					})();
					</script>
					<a href="http://hypercomments.com" class="hc-link" title="comments widget">comments powered by HyperComments</a>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="row">
		  <div class="col-xs-12 col-md-12">
		  	<div class="thumbnail" style="padding-bottom:20px;">
		  		<div class="row" >
				    <div class="col-md-12">
				    	<script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
						<script src="//yastatic.net/share2/share.js"></script>
						<center><div class="ya-share2" data-services="collections,vkontakte,facebook,odnoklassniki,moimir,gplus,twitter"></div></center>
				    </div>
				    <div class="col-md-12">
				    	<center><h3 class="block-heading">@if($row['price']>0){{$row['price']}} рублей @else Товар закончился @endif</h3></center>
					</div>
					<div class="col-md-12">
				    	<ul style="padding:10px 30px;">
				    		<li><span class="glyphicon glyphicon-star" aria-hidden="true" onclick="yaCounter45031696.reachGoal('FAVORITE');"></span> Добавить в избранное</li>
				    		<li><span class="glyphicon glyphicon-bell" aria-hidden="true"></span> Подписка на товар</li>
				    		<li><span class="glyphicon glyphicon-piggy-bank" aria-hidden="true"></span> <a href="/discount">Накопительная скидка</a></li>
				    		<li><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> <a href="/help">Помощь покупателю</a></li>
				    		<li><span class="glyphicon glyphicon-certificate" aria-hidden="true"></span> <a href="/garants">Лучшие гарантии</a></li>
				    	</ul>	
					</div>
					<div class="col-md-12 ">
						<center><a href="http://www.oplata.info/asp/pay_wm.asp?id_d={{$row['sbid']}}&ai=427969" class="btn btn-primary" @if($row['price']==0)disabled="disabled" onclick="return false;" @endif style="padding:10px 70px;font-size:20px" onclick="yaCounter45031696.reachGoal('BUY');">КУПИТЬ</a></center>
					</div>
				</div>
			</div>
		  </div>
		</div>
		<div class="row">	
		 	<div class="col-xs-12 col-md-12">
		  		<div style="padding-bottom:20px;border:0px!important">
		  			<div class="row" >
				    	<div class="col-md-12">
							<script type="text/javascript" src="//vk.com/js/api/openapi.js?146"></script>

							<!-- VK Widget -->
							<div id="vk_groups"></div>
							<script type="text/javascript">
							VK.Widgets.Group("vk_groups", {mode: 3, width: "260px"}, 75046591);
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop