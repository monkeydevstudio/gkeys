@extends('game')
@section('content')
@section('pageTitle','') 
@section('service','Steam') 
@section('description','Интернет-магазин . У нас можно купить лицензионные ключи для игр, ключи STEAM, Origin, Uplay, Battle.net, Minecraft. Купить ключи для STEAM недорого и с моментальной доставкой') 
	
	<style>
		.tname{
			display: inline-block;
			display: inline-block;
		    display: -webkit-inline-box;
		    overflow: hidden;
		    box-sizing: border-box;
		    width: 100%;
		    padding-right: 20px;
		    text-overflow: ellipsis;
		    -webkit-line-clamp: 2;
		    font-size: 24px;
		    font-weight: 400;
		}
		.price{
			margin-right: 3px;
		    line-height: 21px;
		    font-size: 17px;
		    color: #e24920;
		    display:block;
		    margin-bottom: 10px;

		}
		.img{
			width: 100%;
		    max-height: 160px;
		    min-height: 160px;
		    object-fit: cover;
		    border-radius: .25rem .25rem 0rem 0rem;
		}
		label{font-weight: 500!important}
		.ahead{color:#000!important;text-decoration:none;}
	</style>
	<div class="col-md-3"> 
		<form method="GET" action="/search">
			<div class="row">
	  			<div class="col-md-12">
	  				<h5 class="block-heading">Название</h5>
		  			<div class="form-group">
				    	<input type="text" class="form-control" name="title" placeholder="Defcon" value="{{ app('request')->input('title') }}">
				  	</div>
			  	</div>
		  	</div>
		  	<div class="row">
		  		<div class="col-md-12">
		  			<h5 class="block-heading">Цена</h5>
		  		</div>
			  	<div class="col-md-6">
			  		<div class="form-group">
			    		<label for="exampleInputPassword1">от</label>
			    		<input type="text" class="form-control" name="pricefrom" id="exampleInputPassword1" placeholder="50" value="{{ app('request')->input('pricefrom') }}">
			  		</div>
			  	</div>
			  	<div class="col-md-6">
			  		<div class="form-group">
			    		<label for="exampleInputPassword1">до</label>
			    		<input type="text" class="form-control" name="priceto" id="exampleInputPassword1" placeholder="100" value="{{ app('request')->input('priceto') }}">
			  		</div>
			  	</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<h5 class="block-heading">ОС</h5>
				</div>
				<div class="col-md-12">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="Windows" class="os">
              			<label for="input-1">Windows</label>
					</div>
				</div>
				<div class="col-md-12">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="Linux" class="os">
              			<label for="input-1">SteamOS + Linux</label>
					</div>
				</div>
				<div class="col-md-12">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="Mac" class="os">
              			<label for="input-1">Mac OS X</label>
					</div>
				</div>
				<input type="hidden" name="os" value="" />
			</div>

			<div class="row">
				<div class="col-md-12">
					<h5 class="block-heading">Активация</h5>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="battle" class="activity">
              			<label for="input-1">Battle.net</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="psn" class="activity">
              			<label for="input-1">PSN</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="uplay" class="activity"">
              			<label for="input-1">Uplay</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="origin" class="activity">
              			<label for="input-1">Origin</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="steam" class="activity">
              			<label for="input-1">Steam</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="xbox" class="activity">
              			<label for="input-1">Xbox</label>
					</div>
				</div>
				<input type="hidden" name="activity" value="" />
			</div>
			<div class="row">
				<div class="col-md-12">
					<h5 class="block-heading">Жанры</h5>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="arcada" class="genre">
              			<label for="genre">Аркада</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="simulator" class="genre">
              			<label for="input-1">Симулятор</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="action" class="genre">
              			<label for="input-1">Экшн</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="strategy" class="genre">
              			<label for="input-1">Стратегии</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="role" class="genre">
              			<label for="input-1">Ролевые</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="casual" class="genre">
              			<label for="input-1">Казуальные</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="racing" class="genre">
              			<label for="input-1">Гонки</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="sport" class="genre">
              			<label for="input-1">Спорт</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="online" class="genre">
              			<label for="input-1">Онлайн</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="fighting" class="genre">
              			<label for="input-1">Файтинги</label>
					</div>
				</div>
				<div class="col-md-6">	
					<div class="form-group">
						<input tabindex="1" type="checkbox" id="adventure" class="genre">
              			<label for="input-1">Приключения</label>
					</div>
				</div>
				<input type="hidden" name="genre" value="" />
			</div>
  			<button type="submit" class="btn btn-primary btn-block">Поиск</button>
  			<center><a href="/" style="margin-top:15px;display:block;">Сбросить фильтр</a></center>
		</form>

		<script>
			
		</script>
	</div>
	<div class="col-md-9">
		<div class="row">
			@foreach ($row['data'] as $r)
			 
			  <div class="col-sm-4 col-md-4" style="min-height:365px">
			    <div class="thumbnail" style="padding:0px !important;">
			      <a href="/{{$r['params']['slug']}}" class="ahead"><img class="img" src="{{$r['params']['attachment']['preview']}}" alt="Купить ключ для игры {{$r['params']['title']}}"></a>
			      <div class="caption text-center">
			        <h3 style="line-height: 1;max-height: 40px; height:100%!important"><a href="/{{$r['params']['slug']}}" class="ahead">{{$r['params']['title']}}</a></h3>
			        <span class="price">@if($r['params']['price']>0){{$r['params']['price']}} рублей @else Товар закончился @endif</span>
			        <p><a href="/{{$r['params']['slug']}}" class="btn btn-primary" role="button">Подробнее</a></p>
			      </div>
			    </div>
			  </div>
			  
			@endforeach
		</div>
		<div class="row">
			<center>
				@if(isset($row['paginate']))
				<nav aria-label="Page navigation">
				  <ul class="pagination" >
				   	@if($row['current_page']>1) <li>
				      <a href="?page={{$row['paginate']['prev']}}" aria-label="Previous">
				        <span aria-hidden="true">&laquo;</span>
				      </a>
				    </li>
				    @endif
				    @if($row['current_page']>7 && $row['current_page']+7<$row['last_page'])
			    		<li>
				   			<a href="/?page=1">
				   				1
				   			</a>
				   		</li>
				   		<li class="disable">
				   			<a href="#">
				   				...
				   			</a>
				   		</li>
				    	@foreach($row['paginate']['params'] as $p)
						   
						   	@if(isset($p['active']))
						   		<li class="active">
						   			<a href="/?page={{$p['item']}}">
						   				{{$p['item']}}
						   			</a>
						   		</li>
						   	@else
						   		<li>
					   				<a href="/?page={{$p['item']}}">
					   					{{$p['item']}}
					   				</a>
					   			</li>
					   		@endif		
					   	@endforeach
					   	<li class="disable">
				   			<a href="#">
				   				...
				   			</a>
				   		</li>
				   		<li class="disable">
				   			<a href="?page={{$row['last_page']}}">
				   				{{$row['last_page']}}
				   			</a>
				   		</li>		
				    @else
					   	@foreach($row['paginate']['params'] as $p)
						   	@if(isset($p['active']))
						   		<li class="active">
						   			<a href="/?page={{$p['item']}}">
						   				{{$p['item']}}
						   			</a>
						   		</li>
						   	@else
						   		<li>
					   				<a href="/?page={{$p['item']}}">
					   					{{$p['item']}}
					   				</a>
					   			</li>
					   		@endif		
					   			
					   	@endforeach
					@endif
					@if($row['current_page']<$row['paginate']['prev'])
				    <li>
				      <a href="?page={{$row['paginate']['next']}}" aria-label="Next">
				        <span aria-hidden="true">&raquo;</span>
				      </a>
				    </li>
				    @endif
				  </ul>
				</nav>
				 @endif
			</center>
		</div>
	</div>
@stop