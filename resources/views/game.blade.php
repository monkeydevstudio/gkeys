<html>
	<head>
		 <title>Купить ключ для игры @yield('pageTitle')</title>
		 <meta charset="utf-8">
   		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="yandex-verification" content="5726cd89411fc91a" />
    	<meta name="keywords" content="купить @yield('pageTitle'). ключ @yield('pageTitle')" />
    	<meta name="description" content="Вы можете купить лицензионный ключ @yield('pageTitle') по доступной цене, активация игры происходит в сервисе @yield('service'). Мгновенная доставка на электронный адрес, играйте легально!" />
		<meta property="og:type" content="website"/>
    	<meta property="og:site_name" content="GKeys"/>
    	<meta property="og:title" content="Купить ключ для игры @yield('pageTitle')"/>
        <meta property="og:image" content="@yield('image')" />
        <meta property="og:url" content="@yield('url')" />
        <meta property="og:description" content="Вы можете купить лицензионный ключ @yield('pageTitle') по доступной цене, активация игры происходит в сервисе @yield('service'). Мгновенная доставка на электронный адрес, играйте легально!" />
	    <meta name="twitter:card" content="summary_large_image" />
	    <meta name="twitter:title" content="Купить ключ для игры @yield('pageTitle')" />
	    <meta name="twitter:description" content="Вы можете купить лицензионный ключ @yield('pageTitle') по доступной цене, активация игры происходит в сервисе @yield('service'). Мгновенная доставка на электронный адрес, играйте легально!" />
	    <meta name="twitter:url" content="@yield('url')" />
	    <meta name="twitter:image" content=@yield('image')" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="http://bootflat.github.io/css/site.min.css">

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="/js/search.js"></script>
		<script src="/js/icheck.min.js"></script>

		<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
		<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->
		<script type="text/javascript" async src="https://relap.io/api/v6/head.js?token=Q1siNSqu6hfqTElM"></script>

		
	</head>
	<body>
		<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">GKeys.store</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
         	<ul class="nav navbar-nav">
            <li class="active"><a href="/">Игры</a></li>
            <li><a href="/garants">Гарантии</a></li>
            <li><a href="/help">Помощь покупателю</a></li>
            <li><a href="/discount">Накопительная скидка</a></li>
            <li><a href="/favorites">Избранное</a></li>
          </ul>
        </div>
      </div>
    </nav>

  
    <div class="jumbotron htop">
      <div class="container">
       &nbsp;
      </div>
    </div>
		<div class="container">
			<ol class="breadcrumb">
				@if(isset($row['title']))
					<li><a href="/">Игры</a></li>
				  	<li class="active">{{$row['title']}}</li>
				@else 
					<li class="active">Игры</li> 	
				@endif
			</ol>	
			<div class="row">
				@yield('content')
				
			</div>
		</div>
		
    	<style>
    		html, body{
    			background-color:#fff !important;
    		}
			body {
			  padding-top: 50px;

			}
			.text-muted {
				margin: 20px 0;	
    			color: #777;
    			text-align:center;
			}
			.footer {
			    position: relative;
			    bottom: 0;
			    width: 100%;
			    height: 7em;
			    background-color: #f5f5f5;
			}
			.htop{
				background-image: url("http://www.arkfalls.com/wp-content/uploads/2013/03/world-header-bg.jpg" );
				background-size: cover;
				min-height:331px;

			}
			.pagination>.active>span{
				background-color:#3bafda;
				border-color:#3bafda; 
			}
		</style>
		<script>
			$(document).ready(function(){
			  $('input').iCheck({
			    checkboxClass: 'icheckbox_flat',
			    radioClass: 'iradio_flat'
			  });
			});
		</script>
    	<!-- Yandex.Metrika counter --> <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter45031696 = new Ya.Metrika({ id:45031696, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/45031696" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
    	<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-101352725-1', 'auto');
		  ga('send', 'pageview');

		</script>
		<footer class="footer">
      		<div class="container">
      			<div class="row">
       				<center><p class="text-muted">Магазин игр GKEYS.STORE. Все права защищены. ©gkeys.store by <a href="http://monkeydevstudio.ru" target="_blank">Monkey Developers Studio</a></p></center>
      			</div>
      		</div>
    	</footer>
	</body>
</html>