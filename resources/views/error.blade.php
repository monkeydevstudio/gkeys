<html>
	<head>
		 <title>@yield('pageTitle')</title>
		 <meta charset="utf-8">
   		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
    	<meta name="yandex-verification" content="5726cd89411fc91a" />
		<meta property="og:type" content="website"/>
    	<meta property="og:site_name" content="GKeys"/>
    	<meta property="og:title" content="@yield('pageTitle')"/>
        <meta property="og:image" content="@yield('image')" />
        <meta property="og:url" content="@yield('url')" />
        <meta property="og:description" content="@yield('description')" />
	    <meta name="twitter:card" content="summary_large_image" />
	    <meta name="twitter:title" content="@yield('pageTitle')" />
	    <meta name="twitter:description" content="@yield('description')" />
	    <meta name="twitter:url" content="@yield('url')" />
	    <meta name="twitter:image" content=@yield('image')" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="http://bootflat.github.io/css/site.min.css">

		<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<script src="/js/search.js"></script>
		<script src="/js/icheck.min.js"></script>

		<link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
		<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->
		<script type="text/javascript" async src="https://relap.io/api/v6/head.js?token=Q1siNSqu6hfqTElM"></script>
	</head>
	<body>
		@yield('content')
	</body>
</html>		