<?php echo '<?xml version="1.0" encoding="UTF-8"?>'."\n"; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	@foreach($row as $r)
		<url>
			<loc>http://gkeys.store/{{$r->slug}}</loc>
			<priority>0.6</priority>
		</url>
	@endforeach
</urlset> 