<?php echo '<?xml version="1.0" encoding="UTF-8"?>'."\n"; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<url>
		<loc>http://gkeys.store</loc>
		<priority>1</priority>
	</url>
	<url>
		<loc>http://gkeys.store/garants</loc>
		<priority>0.6</priority>
	</url>
	<url>
		<loc>http://gkeys.store/help</loc>
		<priority>0.6</priority>
	</url>
	<url>
		<loc>http://gkeys.store/discount</loc>
		<priority>0.6</priority>
	</url>
	@foreach($row as $r)
		<url>
			<loc>http://gkeys.store/{{$r->slug}}</loc>
			<priority>0.6</priority>
		</url>
	@endforeach
</urlset> 