$( document ).ready(function(){
	function getParams(name, url) {
	    if (!url) url = window.location.href;
	    name = name.replace(/[\[\]]/g, "\\$&");
	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	        results = regex.exec(url);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, " "));
	}


	$(function(){
		var os=decodeURI(getParams('os')).split(',');
		var genre=decodeURI(getParams('genre')).split(',');
		var activity=decodeURI(getParams('activity')).split(',');
		if(os.length>0){
			$.each(os,function(i,v){
				if(v!==''){
					$('#'+v).iCheck('check');
				}
			});
		}
		if(genre.length>0){
			$.each(genre,function(i,v){
				if(v!==''){	
					$('#'+v).iCheck('check');
				}
			});
		}
		if(activity.length>0){
			$.each(activity,function(i,v){
				if(v!==''){
					$('#'+v).iCheck('check');
				}
			});
		}
	});
	$('input').on('ifChanged', function(event){
		var type=event.target.className;
		var arr=[];
		arr=$('input[name='+type+']').val().split(',');
		if(event.target.checked===true){
			var val=event.target.attributes.id.value;
			arr.push(val);
			$('input[name='+type+']').val(arr.join(','));
			
		}else{
			var val=event.target.attributes.id.value;
			Array.prototype.remove=function(el){
				return this.splice(this.indexOf(el),1);
			};
			arr.remove(val);
			$('input[name='+type+']').val(arr.join(','));
		}
	});
});